# portainer information

## Configuration files
Custom configuration to allow this to be fairly is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

A volume named ```portainer_data``` will need to be created beforehand.

## File content
The variables used in the .env file are as follows:

```portainer_url``` -> the URL that Porainer will be hosted on for referencing the Let's Encrypt certificate  

## Final steps
Before deploying this you will need to create the Docker volume. This can be done with the command ```docker volume create portainer_data```  

A certificate from Let's Encrypt will be required separately from the container for this configuration setup. The configuration is designed around using Certbot (installed from a Snap on Ubuntu in my case). This places the certificates in the directory ```/etc/letsencrypt/live/<domain name>```. There is an additional mount for the directory ```/etc/letsencrypt/archive/<domain name>``` since the certificates in live are sym-linked to the archive directory. On starting the Portainer container the certifiates are copied from the Certbot location to the ```/certs/``` directory for actual use.

## Note
To deploy the Community Edition of Portainer simply replace the image line in the ```docker-compose.yml``` file with ```image: portainer/portainer-ce```