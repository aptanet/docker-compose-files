# dokuwiki information

## Configuration files
Custom configuration to allow this to be fairly generic for use on multiple sites is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

A volume named to match the ```config_volume``` name used in the ```.env``` file will need to be created beforehand.

## File content
The variables used in the .env file are as follows:

```container_name``` -> this is a name for the individual container  
```config_volume``` -> a pre-created volume for the NGINX configuration is required, put the name here  
```dokuwiki_url``` -> this is the URL of the website being hosted  
```dokuwiki_certresolver``` -> this is the name of the certresolver to be used, this is defined in the traefik.yml file of the Traefik installation being used as the reverse proxy  