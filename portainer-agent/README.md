# portainer-agent information

## Configuration files
Custom configuration to allow this to be fairly is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

## File content
The variables used in the .env file are as follows:

```EDGEID``` -> the Edge ID as created in the main Portainer install for the connection  
```EDGEKEY``` -> the Edge Key as created in the main Portainer install for the connection  

## Final steps
Before deploying this you will need the variables for the ```.env``` file. These are created from your main Portainer install.  

- Log into Portainer head to ```Environments```
- Click on the ```Add environment``` button towards the top right
- Choose ```Docker Standalone``` and ```Start Wizard``` (at the bottom)
  - Choose ```Edge Agent Standard```
  - Provide a suitable name
  - Click ```Create``` (at the bottom)
- From here you can:
  - copy the join token provided (which can also be found as the ```EDGE_KEY``` in the provided docker run command towards the bottom)
  - copy the ```EDGE_ID``` (which can be found in the provided docker run command towards the bottom)