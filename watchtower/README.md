# watchtower information

## Configuration files
Custom configuration to allow this to be fairly is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

## File content
The variables used in the .env file are as follows:

```docker_host``` -> hostname for Docker container  
```monitor_only``` -> this is set to true to only download image updates not deploy them  
```schedule``` -> defines the schedule to run checks using cron style formating (example uses daily at 11.30pm)  
```notifications``` -> notification types (example uses email and Discord via Shoutrrr)  
```email_to``` -> the email address you want notifications to be sent to  
```email_from``` -> the email address you want notifications to come from  
```email_server``` -> your smtp mail server
```email_server_user``` -> username for email sending authentication  
```email_server_pass``` -> user password for email sending authentication
```email_port``` ->  mail server port
```notification_url``` -> URL for Discord notifications

## Final steps
The Discord notification URL is in the format ```discord://<discord_key>```  

- to create the key choose the channel you want to send messages to on your own Discord server and select ```Edit Channel -> Integrations -> Webhooks```
- from there click on ```New Webhook```
- then edit the Webhook my clicking on the```>``` where you can change the name and choose the channel it can post to
- once this is done you can copy the Webhook URL
- use the last section of this (after the final ```/```) as the <discord_key> in the ```.env``` file

The docker_host is used to set the hostname for the container. This makes identifying which server the Watchtower notification from easier when you are running more than one.