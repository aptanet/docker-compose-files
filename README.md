# Docker compose files

## Name
Docker Compose Files

## Description
This is a simple collection of Docker Compose files that I have used with the Gitlab integration of Portainer. They should also work as docker-compose.yml file, but have not at this point been tested. They are public versions that I can refer to when discussing in public forums, mailing lists, etc.

## Details
Feel free to make use of them, but please make sure you understand them and don't simply deploy as they are. The intent is more of a discussion / educational resource and not a straight drop in deployment resource. That is good advice for pretty much any resource on the internet though as software rarely stands still, information can be outdated, and simply running a script or copying a configuration file without understanding what it will do leaves you open to all sorts of problems.

Currently available Docker Compose setups are:

- audiobookshelf -> Audiobookshelf for hosting audiobooks and podcasts
- dokuwiki -> Dokuwiki for hosting database free wiki site
- homer -> web dashboard
- nginx-static -> NGINX setup for hosting static websites
- portainer-agent -> Portainer agent for connecting to a main Portainer instance
- portainer -> Portainer Docker management
- traefik -> Traefik reverse proxy
- watchtower -> Watchtower service for notifying of updates to Docker images

## Note
The following containers require the Traefik container for the configuration used:

- audiobookshelf
- dokuwiki
- homer
- nginx-static

Paul Tansom  
25 October 2023