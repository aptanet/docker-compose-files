# traefik information

## Configuration files
Custom configuration to allow this to be fairly is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

A volume named ```traefik_config``` will need to be created beforehand.

## File content
The variables used in the .env file are as follows:

```dashboard_url``` -> this is the URL for the Traefik dashboard  
```dashboard_whitelist``` -> this is this is a list of the IP addresses allowed to access the dashboard (comma separated IPs or ranges of allowed IPs by using CIDR notation)  
```dashboard_certresolver``` -> this is the name of the certresolver to be used, this is defined in the traefik.yml file of the Traefik installation being used as the reverse proxy  
```DO_AUTH_TOKEN``` -> this is for use with a Digital Ocean DNS Let's Encrypt challenge for TLS certificates (for other challenges other environment variables will have to be created accordingly)  

## Final steps
An example ```traefik.yml``` file is included to get you started.
- the email will need replacing with the email you have registered with Let's Encrypt
- there are four certresolvers configured:
  - live and staging web challenge resolvers
  - live and statring DNS challenge resolvers using Digital Ocean (which can be used behind a firewall)
- a keytype of EC384 is used for ECDSA TLS certificates
- http is automatically redirected to https
- the ```traefik.yml``` file should be copied into the traefik_config volume which is mounted to ```/etc/traefik```
- a password file called ```dashboard.pwd``` will need to be created
  - the password can be created with the command ```mkpasswd -m bcrypt```