# audiobookshelf information

## Configuration files
Custom configuration to allow this to be fairly generic for use on multiple sites is stored in the .env file.

For use with command line Docker Compose use the filename ```.env```.

For use with Portainer upload the environment variables into the Stack, in which case the filename is flexible and can be named using a more useful name for local storage.

Volumes named to match the ```audiobookshelf_config``` and ```audiobookshelf_metadata``` names used in the ```.env``` file will need to be created beforehand.

## File content
The variables used in the .env file are as follows:

```audiobookshelf_url``` -> this is the URL of the website being hosted  
```audiobookshelf_certresolver``` -> this is the name of the certresolver to be used, this is defined in the traefik.yml file of the Traefik installation being used as the reverse proxy  
```audiobookshelf_audiobooks``` -> bind mount for audiobook storage  
```audiobookshelf_podcasts``` -> bind mount for poscast storage  